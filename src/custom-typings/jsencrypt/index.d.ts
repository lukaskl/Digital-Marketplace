declare module 'jsencrypt' {


    class BigInteger {

        toString(radix): BigInteger;
        negate(): BigInteger
        abs(): BigInteger
        compareTo(other: BigInteger): number;
        bitLength(): number;
        mod(a): BigInteger;
        modPowInt(e, m);

        // "constants"
        static readonly ZERO;
        static readonly ONE;
    }

    /**
     *
     * @param {Object} [options = {}] - An object to customize JSEncrypt behaviour
     * possible parameters are:
     * - default_key_size        {number}  default: 1024 the key size in bit
     * - default_public_exponent {string}  default: '010001' the hexadecimal representation of the public exponent
     * - log                     {boolean} default: false whether log warn/error or not
     * @constructor
     */
    interface JSEncryptOptions {
        default_key_size?: number;
        default_public_exponent?: string;
        log?: boolean;
    }

    class JSEncrypt {
        constructor(options?: JSEncryptOptions);

        /**
         * Method to set the rsa key parameter (one method is enough to set both the public
         * and the private key, since the private key contains the public key paramenters)
         * Log a warning if logs are enabled
         * @param {Object|string} key the pem encoded string or an object (with or without header/footer)
         * @public
         */
        public setKey(key: string | RSAKey | RSAKeyPublic): void;

        /**
         * Proxy method for RSAKey object's decrypt, decrypt the string using the private
         * components of the rsa key object. Note that if the object was not set will be created
         * on the fly (by the getKey method) using the parameters passed in the JSEncrypt constructor
         * @param {string} string base64 encoded crypted string to decrypt
         * @return {string} the decrypted string
         * @public
         */
        public decrypt(value: string): string;

        /**
         * Proxy method for RSAKey object's encrypt, encrypt the string using the public
         * components of the rsa key object. Note that if the object was not set will be created
         * on the fly (by the getKey method) using the parameters passed in the JSEncrypt constructor
         * @param {string} string the string to encrypt
         * @return {string} the encrypted string encoded in base64
         * @public
         */
        public encrypt(value: string): string;

        /**
         * Getter for the current JSEncryptRSAKey object. If it doesn't exists a new object
         * will be created and returned
         * @param {callback} [cb] the callback to be called if we want the key to be generated
         * in an async fashion
         * @returns {JSEncryptRSAKey} the JSEncryptRSAKey object
         * @public
         */
        public getKey(callback?: Function): JSEncryptRSAKey;

        /**
        * Returns the pem encoded representation of the private key
        * If the key doesn't exists a new key will be created
        * @returns {string} pem encoded representation of the private key WITH header and footer
        * @public
        */
        public getPrivateKey(): string;

        /**
         * Returns the pem encoded representation of the public key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the public key WITH header and footer
         * @public
         */
        public getPublicKey(): string;


        /**
         * Returns the pem encoded representation of the public key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the public key WITHOUT header and footer
         * @public
         */
        public getPublicKeyB64(): string;

        /**
         * Returns the pem encoded representation of the private key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the private key WITHOUT header and footer
         * @public
         */
        public getPrivateKeyB64(): string;
    }


    interface RSAKeyPublic {
        n: BigInteger;
        e: number;

        /**
         * Returns the pem encoded representation of the public key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the public key WITH header and footer
         * @public
         */
         getPublicKey(): string;


        /**
         * Returns the pem encoded representation of the public key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the public key WITHOUT header and footer
         * @public
         */
         getPublicKeyB64(): string;

    }

    interface RSAKey extends RSAKeyPublic {
        d: BigInteger;
        p: BigInteger;
        q: BigInteger;
        dmp1: BigInteger;
        dmq1: BigInteger;
        coeff: BigInteger;

         /**
        * Returns the pem encoded representation of the private key
        * If the key doesn't exists a new key will be created
        * @returns {string} pem encoded representation of the private key WITH header and footer
        * @public
        */
        getPrivateKey(): string;


        /**
         * Returns the pem encoded representation of the private key
         * If the key doesn't exists a new key will be created
         * @returns {string} pem encoded representation of the private key WITHOUT header and footer
         * @public
         */
        getPrivateKeyB64(): string;
    }

    interface JSEncryptRSAKey extends RSAKey {

        /**
         * Create a new JSEncryptRSAKey that extends Tom Wu's RSA key object.
         * This object is just a decorator for parsing the key parameter
         * @param {string|Object} key - The key in string format, or an object containing
         * the parameters needed to build a RSAKey object.
         * @constructor
         */
        constructor(key: string | RSAKey | RSAKeyPublic);
    }

}
