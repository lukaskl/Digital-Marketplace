
import { JSEncrypt } from "jsencrypt";
export class AccountSecret {


  private rsaKeys: string[] = [];

  constructor(
    public displayName: string,
    public publicAddress: string,
    public privateKeyWIF: string) {
  }

  assignKey(key: string) {
    if (key === undefined) {
      throw new Error("cannot add empty key");
    }
    this.rsaKeys.push(key);
  }

  isOwnerOfKey(publicKey: string) {
    return this.rsaKeys.find(key => key === publicKey) !== undefined;
  }

  getRsaPublicKeys(){
    return this.rsaKeys;
  }
}
