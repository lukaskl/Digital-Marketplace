
import { IEvent } from "app/event-sourcing/events/event";

export class Block {

  constructor(
    public hash: string,
    public height: number,
    public time: Date,
    public nonce: number,
    public previousHash: string,
    public events: string[]) { }


  getData(): string {
    return this.nonce + this.getDataWithoutNonce();
  }

  getDataWithoutNonce(): string {
    return "" + this.height + this.time.getTime() + this.previousHash + this.events.join(",");
  }
}
