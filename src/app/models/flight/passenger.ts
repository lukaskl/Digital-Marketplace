// someone who travels
export class Passenger {
  name: string;
  surname: string;
  nationality: string;
  age: number;
  passportId: string;
}