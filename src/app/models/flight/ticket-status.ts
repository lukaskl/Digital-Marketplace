
export enum TicketStatus {
  Offered,
  Purchased,
  Canceled,
  Conducted
}
