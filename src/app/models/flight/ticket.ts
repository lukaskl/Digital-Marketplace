import { Customer } from "app/models/flight/customer";
import { Passenger } from "app/models/flight/passenger";
import { TicketStatus } from "app/models/flight/ticket-status";

export class Ticket {
  hashId: string;
  price: number;
  seat: string;
  departureTime: Date;
  arrivalTime: Date;
  departure: string;
  arrival: string;
  secretPayload: string;
  carrierPublicRsaKey: string;
  customerAccount: string;
  state: TicketStatus;
  carrierAccount: string;
}
