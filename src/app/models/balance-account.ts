export class BalanceAccount {

  constructor(
    public publicAddress: string,
    public balance: number) { }
}
