import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";

import { CryptoService } from "app/services/crypto.service";
import { BalanceAccount } from "app/models/balance-account";


@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss"],
})
export class AccountComponent implements OnInit {


  @Input() account: BalanceAccount;
  constructor() { }


  get payload(): string {
    return JSON.stringify(this.account);
  }

  ngOnInit() {
  }

}
