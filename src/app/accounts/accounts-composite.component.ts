import { Component } from "@angular/core";

@Component({
  selector: "app-accounts-composite",
  templateUrl: "./accounts-composite.component.html",
  styleUrls: ["./accounts-composite.component.scss"],
})
export class AccountsCompositeComponent {}
