import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";

import { BalanceAccount } from "app/models/balance-account";
import { AccountsService } from "app/services/accounts.service";

@Component({
  selector: "app-accounts",
  templateUrl: "./accounts.component.html",
  styleUrls: ["./accounts.component.scss"],
})
export class AccountsComponent implements OnInit {

  accounts: BalanceAccount[] = [];

  constructor(
    private accountsService: AccountsService
  ) { }

  async ngOnInit() {
    this.accounts = await this.accountsService.getAccounts();
  }
}
