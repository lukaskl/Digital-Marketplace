import { Component, Input, OnInit } from "@angular/core";

import { SecretTicketPurchasedPayload } from "../event-sourcing/events/ticket-purchased";
import { Passenger } from "../models/flight/passenger";
import { Ticket } from "../models/flight/ticket";
import { AccountSecretsService } from "../services/accountSecrets.service";
import { Settings } from "../settings";
import { Customer } from "app/models/flight/customer";


@Component({
  selector: "app-ticket",
  templateUrl: "./ticket.component.html",
  styleUrls: ["./ticket.component.scss"],
})
export class TicketComponent implements OnInit {

  private secretPayload: SecretTicketPurchasedPayload;

  @Input() ticket: Ticket;
  constructor(private settings: Settings,
    private accountSecrets: AccountSecretsService) { }


  ngOnInit() {
    this.decrypt();
  }

  async decrypt() {
    if (this.settings.asSeenBy === this.ticket.carrierAccount && this.ticket.secretPayload) {

      const canBeDecrypted = await this.accountSecrets.isOwnerOfRsaKey(this.ticket.carrierAccount, this.ticket.carrierPublicRsaKey);
      if (!canBeDecrypted) {
        return;
      }

      const rsaKey = this.accountSecrets.getRsaKey(this.ticket.carrierPublicRsaKey);
      this.secretPayload = JSON.parse(rsaKey.decrypt(this.ticket.secretPayload)) as SecretTicketPurchasedPayload;
    }
  }

  getState(): string {
    switch (this.ticket.state) {
      case 0:
        return "Announced";
      case 1:
        return "Purchased";
      case 2:
        return "Cancelled";
      default:
        return "Unknown";
    }
  }

}
