import { Component, OnInit } from "@angular/core";

import { Ticket } from "../models/flight/ticket";
import { TicketsService } from "../services/tickets.service";


@Component({
  selector: "app-tickets",
  templateUrl: "./tickets.component.html",
  styleUrls: ["./tickets.component.scss"],
})
export class TicketsComponent implements OnInit {

  tickets: Ticket[] = [];

  constructor(
    private ticketsService: TicketsService
  ) { }

  async ngOnInit() {
    this.tickets = await this.ticketsService.getTickets();
  }
}
