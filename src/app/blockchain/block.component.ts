import { Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { BlockContext } from "app/event-sourcing/contexts/block-context";


@Component({
  selector: "app-block",
  templateUrl: "./block.component.html",
  styleUrls: ["./block.component.scss"],
})
export class BlockComponent implements OnInit, OnChanges, DoCheck {

  private _blockStateCache: string;
  private _previousIsValidState: boolean;
  errorsMessages: string[] = [];

  @Input() context: BlockContext;
  @Output() isValidChanged = new EventEmitter<BlockContext>();

  constructor() { }

  get events(): string {
    return JSON.stringify(this.context.block.events, undefined, 2);
  }
  set events(value: string) {
    try {
      this.context.block.events = JSON.parse(value);
    } catch (err) {
    }
  }

  mineBlock() {

    this.context.mineNonce();
  }

  ngOnInit() { }

  calculateHash(): string {
    return this.context.calculateHash();
  }

  copyCalculatedHash() {
    this.context.block.hash = this.context.calculateHash();
  }

  isHashValid(): boolean {
    return this.context.calculateHash() === this.context.block.hash;
  }

  get isValid(): boolean {
    const currentState = this.errorsMessages.length === 0;

    if (this._previousIsValidState !== currentState) {
      this.isValidChanged.emit(this.context);
      this._previousIsValidState = currentState;
    }

    return currentState;
  }

  ngDoCheck(): void {
    const blockState = JSON.stringify(this.context.block);
    if (this._blockStateCache !== blockState) {
      this.context.validator.getValidationErrors()
        .then(x => {
          this.errorsMessages = x;
        });
      this._blockStateCache = blockState;
    }
  }

  ngOnChanges(changes) {
    this.recalculateBlockState();
  }

  recalculateBlockState() {
    this._blockStateCache = undefined;
  }
}
