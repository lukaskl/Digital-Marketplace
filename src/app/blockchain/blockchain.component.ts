import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { BlockComponent } from "app/blockchain/block.component";
import { BlockContext } from "app/event-sourcing/contexts/block-context";
import { BlockchainService } from "app/services/blockchain.service";


@Component({
  selector: "app-blockchain",
  templateUrl: "./blockchain.component.html",
  styleUrls: ["./blockchain.component.scss"],

})
export class BlockchainComponent implements OnInit {


  @ViewChildren("blockComponent") blockComponents: QueryList<BlockComponent>;

  blocks: BlockContext[] = [];
  constructor(private blocksService: BlockchainService) { }



  async ngOnInit() {
    this.blocks = await this.blocksService.getBlocks();
  }

  mineNewBlock() {
    this.blocksService.mineNewBlock();
  }

  onBlockStateChanged(block: BlockContext) {
    this.recalculateState(block);
  }

  private recalculateState(startingBlock: BlockContext) {
    if (this.blockComponents && this.blockComponents.length > 0) {

      const childComponents = this.blockComponents.toArray();

      for (let i = childComponents.length - 1; i > 0; i--) {
        const blockCmp = childComponents[i];
        if (blockCmp.context === startingBlock) {
          return;
        } else {
          blockCmp.recalculateBlockState();
        }
      }
    }
  }
}
