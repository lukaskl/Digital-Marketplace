import { IEvent } from "app/event-sourcing/events/event";
import { Block } from "app/models/block";

interface IEventContext<TEvent extends IEvent<any>> {
  event: TEvent;
  confirmedInBlock?: Block;
  isPublished?: boolean;
}


// tslint:disable-next-line:no-unused-expression
export class EventContext<TEvent extends IEvent<any>>{

  event: TEvent;
  confirmedInBlock: Block;
  isPublished: boolean;

  constructor(obj: IEventContext<TEvent>) {
    this.event = obj.event;
    this.confirmedInBlock = obj.confirmedInBlock;
    this.isPublished = obj.isPublished;
  }

  get isConfirmed(): boolean {
    return this.confirmedInBlock && this.confirmedInBlock.events.some(x => x === this.event.hash);
  }

  // tslint:disable-next-line:semicolon
}
