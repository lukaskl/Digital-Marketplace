import { Block } from "app/models/block";
import { BlockValidatorBase } from "app/services/validation/block-validator";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { IEvent } from "app/event-sourcing/events/event";
import { EventsService } from "app/services/events.service";
import { Utils } from "utils";
import { CryptoService } from "app/services/crypto.service";
import { BlockchainService } from "app/services/blockchain.service";
import { LoadContext } from "app/event-sourcing/contexts/load-context";

export class BlockContext {

  private _events: LoadContext<EventContext<IEvent<any>>>;
  public unableToLoadEvents: string[] = [];
  public validator: BlockValidatorBase;

  constructor(
    public block: Block,
    validatorFactory: (BlockContext) => BlockValidatorBase,
    private blockchainService: BlockchainService,
    private eventsService: EventsService,
    private crypto: CryptoService

  ) {
    this.validator = validatorFactory(this);
  }

  async getEvents(): Promise<LoadContext<EventContext<IEvent<any>>>> {
    if (!Utils.arraysEqual(this._events && this._events.loadedElements.map(x => x.event.hash),
     this.block.events)) {
      this._events = await this.loadEvents(this.block.events);
    }
    return Promise.resolve(this._events);
  }

  async getPreviousBlock(): Promise<BlockContext> {

    return this.blockchainService.getBlock(this.block.previousHash);
  }
  calculateHash(): string {
    return this.crypto.getHash(this.block.getData());
  }

  mineNonce() {
    this.block.nonce = this.crypto.mineNonce(this.block.getDataWithoutNonce())
    this.block.hash = this.calculateHash();
  }

  private async loadEvents(hashes: string[]): Promise<LoadContext<EventContext<IEvent<any>>>> {
    if (!hashes) { hashes = []; }

    const result: LoadContext<EventContext<IEvent<any>>> = {
      invalidHashes: [],
      loadedElements: []
    };

    this.unableToLoadEvents = [];

    for (const hash of hashes) {
      try {
        const event = await this.eventsService.getEvent(hash);
        result.loadedElements.push(event);
      } catch (err) {
         result.invalidHashes.push(hash);
      }
    }

    return result;
  }


}
