
export class LoadContext<T>{
  loadedElements: T[];
  invalidHashes: string[];
}
