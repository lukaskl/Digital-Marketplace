import { ISignableEvent } from "app/event-sourcing/events/event";

export interface TicketAnnouncedPayload {
  price: number;
  flightNumber: string;
  seat: string;
  departureTime: Date;
  arrivalTime: Date;
  departure: string;
  arrival: string;
  carrierAccount: string;
  publicRsaKey: string;
}

export class TicketAnnounced implements ISignableEvent<TicketAnnouncedPayload> {

  static TypeName = "TicketAnnounced";
  get type(): string { return TicketAnnounced.TypeName; }

  constructor(
    public payload: TicketAnnouncedPayload,
    public createdOn: Date,
    public hash: string,
    public signature: string
  ) { }
}
