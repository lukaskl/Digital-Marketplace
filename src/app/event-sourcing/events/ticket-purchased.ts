import { ISignableEvent } from "app/event-sourcing/events/event";
import { Customer } from "app/models/flight/customer";
import { Passenger } from "app/models/flight/passenger";

export interface TicketPurchasedPayload {

  // hash of the announced ticket
  ticket: string;
  customerAccount: string;
  //encrypted private payload
  secretPayload: string,
}

export interface SecretTicketPurchasedPayload {
  passenger: Passenger;
  customer: Customer;
}

export class TicketPurchased implements ISignableEvent<TicketPurchasedPayload> {
  static TypeName = "TicketPurchased";
  get type(): string { return TicketPurchased.TypeName; }

  constructor(
    public payload: TicketPurchasedPayload,

    public createdOn: Date,
    public hash: string,
    public signature: string
  ) { }
}
