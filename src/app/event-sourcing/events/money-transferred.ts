import { IEvent, ISignableEvent } from "app/event-sourcing/events/event";

export class MoneyTransferredPayload {
  amount: number;
  transactionFee: number;
  from: string;
  to: string;
}

export class MoneyTransferred implements ISignableEvent<MoneyTransferredPayload> {

  static readonly TypeName = "MoneyTransferred";

  constructor(
    public payload: MoneyTransferredPayload,
    public createdOn: Date,
    public hash: string,
    public signature: string) {
  }

  get type(): string {
    return MoneyTransferred.TypeName;
  }
}

export class CoinbaseCreatedPayload {
  amount: number;
  to: string;
}

export class CoinbaseCreated implements IEvent<CoinbaseCreatedPayload> {


  static readonly TypeName = "CoinbaseCreated";

  get type() {
    return CoinbaseCreated.TypeName;
  }

  constructor(
    public payload: CoinbaseCreatedPayload,
    public createdOn: Date,
    public hash: string
  ) { }
}

