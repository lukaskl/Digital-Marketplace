import { ISignableEvent } from "app/event-sourcing/events/event";

export interface TicketCancelledPayload {

  // hash of the announced ticket
  ticket: string;
  reason: string;
}

export class TicketCancelled implements ISignableEvent<TicketCancelledPayload> {

  static TypeName = "TicketCancelled";
  get type(): string { return TicketCancelled.TypeName; }

  constructor(
    public payload: TicketCancelledPayload,
    public createdOn: Date,
    public hash: string,
    public signature: string
  ) { }
}

