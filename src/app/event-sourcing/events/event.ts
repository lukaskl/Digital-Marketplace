export interface IEvent<TPayload> {
  readonly type: string;

  createdOn: Date;
  payload: TPayload;

  // stands as ID of the event
  hash: string;

}


export interface ISignableEvent<TPayload> extends IEvent<TPayload> {
  signature: string;
}

