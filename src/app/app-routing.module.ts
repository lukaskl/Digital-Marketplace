import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AccountsCompositeComponent } from "app/accounts/accounts-composite.component";
import { EventsComponent } from "app/events/events.component";

import { BlockchainComponent } from "./blockchain/blockchain.component";
import { TicketsComponent } from "./tickets/tickets.component";

const routes: Routes = [
  { path: "", redirectTo: "/blockchain", pathMatch: "full" },
  { path: "blockchain", component: BlockchainComponent },
  { path: "events", component: EventsComponent },
  { path: "accounts", component: AccountsCompositeComponent },
  { path: "tickets", component: TicketsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
