import { Component, OnInit } from "@angular/core";

import { AccountSecret } from "../models/account.secret";
import { AccountSecretsService } from "../services/accountSecrets.service";

@Component({
  selector: "app-account-secrets",
  templateUrl: "./account-secrets.component.html",
  styleUrls: ["./account-secrets.component.scss"],
})
export class AccountSecretsComponent implements OnInit {

  accounts: AccountSecret[] = [];

  constructor(
    private accountSecretsService: AccountSecretsService
  ) { }

  async ngOnInit() {
    this.accounts = await this.accountSecretsService.getAccountSecrets();
  }
}
