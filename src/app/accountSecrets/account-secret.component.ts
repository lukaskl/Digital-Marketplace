import { Component, Input, OnInit } from "@angular/core";

import { AccountSecret } from "../models/account.secret";
import { AccountSecretsService } from "../services/accountSecrets.service";
import { JSEncrypt } from "jsencrypt";


@Component({
  selector: "app-account-secret",
  templateUrl: "./account-secret.component.html",
  styleUrls: ["./account-secret.component.scss"],
})
export class AccountSecretComponent {


  @Input() account: AccountSecret;
  constructor(private accountSecrets: AccountSecretsService) { }

  getRsaKeys(): JSEncrypt[] {
    const keys = this.account.getRsaPublicKeys().map(x=> this.accountSecrets.getRsaKey(x));
    return keys;
  }

}
