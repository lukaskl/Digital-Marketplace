import { Injectable } from "@angular/core";

@Injectable()
export class Settings {

  difficulty = 4;
  coinbaseAddress = "A346Ls/3sAFayQTdV9E0FvvPVDAvDwVNmbJzBLEal3+3";
  coinbaseAmount = 50;
  genesisBlockPreviousHash = "0000000000000000000000000000000000000000000000000000000000000000";
  openSslKeySize = 512 * 2 * 2 ;
  asSeenBy = "A346Ls/3sAFayQTdV9E0FvvPVDAvDwVNmbJzBLEal3+3";
}
