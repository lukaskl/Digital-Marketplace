import { TicketCancelledHandler } from "./event-handlers/flight/ticket-cancelled.handler";
import { TicketPurchasedHandler } from "./event-handlers/flight/ticket-purchased.handler";
import { Injectable } from "@angular/core";
import { IEvent } from "app/event-sourcing/events/event";
import { CoinbaseCreatedHandler } from "app/services/event-handlers/coinbase-created.handler";
import { EventHandler } from "app/services/event-handlers/event-handler";
import { TicketAnnouncedHandler } from "app/services/event-handlers/flight/ticket-announced.handler";
import { MoneyTransferredHandler } from "app/services/event-handlers/money-transferred.handler";



@Injectable()
export class EventHandlersServices {

  private eventHandlers: EventHandler<any>[] = [];

  constructor(
    moneyTransfers: MoneyTransferredHandler,
    coinbaseCreations: CoinbaseCreatedHandler,

    ticketAnnouncements: TicketAnnouncedHandler,
    ticketPurchases: TicketPurchasedHandler,
    ticketCancellations: TicketCancelledHandler,
  ) {

    this.eventHandlers.push(moneyTransfers);
    this.eventHandlers.push(coinbaseCreations);

    this.eventHandlers.push(ticketAnnouncements);
    this.eventHandlers.push(ticketPurchases);
    this.eventHandlers.push(ticketCancellations);
  }

  getEventHandler(event: IEvent<any>): EventHandler<IEvent<any>> {

    const handler = this.eventHandlers.find(x => x.eventType === event.type);

    if (!handler) {
      throw new Error("Event handler for events of type '" + event.type + "' is not yet implemented");
    }

    return handler;
  }
}
