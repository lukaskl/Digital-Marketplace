import { Injectable } from "@angular/core";
import sha256, { Hash, HMAC } from "fast-sha256";
import { TextEncoder } from "text-encoding";
import { Settings } from "../settings";
import { ECPair, ECSignature, crypto, bufferutils, address, networks } from "bitcoinjs-lib";
import { AccountSecret } from "app/models/account.secret";
import { JSEncrypt } from "jsencrypt";

declare const Buffer;

// web workers support:
// https://github.com/angular/angular-cli/issues/5885

@Injectable()
export class CryptoService {

  private encoder: TextEncoder;

  constructor(private settings: Settings) {
    this.encoder = new TextEncoder();
  }

  generateAddress(displayName?: string): AccountSecret {
    const keys = ECPair.makeRandom({});

    // NOTE: originally address should be ran through hash160 algorithm
    // we are skipping this step to simplify our process
    const address = keys.getPublicKeyBuffer().toString("base64");
    const secret = keys.toWIF();

    if (displayName === undefined) {
      displayName = keys.getPublicKeyBuffer().toString('hex').substring(0, 10);
    }
    return new AccountSecret(displayName, address, secret);
  }

  generateRsaKey(): JSEncrypt {
    const key = new JSEncrypt({
      default_key_size: this.settings.openSslKeySize
    });

    return key;
  }



  sign(hash: string, secret: string): string {
    const buffer = new Buffer(hash, "hex");
    const keys = ECPair.fromWIF(secret);
    const signature = keys.sign(buffer) as ECSignature;

    const der64 = signature.toDER().toString("base64");
    return der64;
  }

  verify(hash: string, publicKey: string, signature: string): boolean {
    try {
      const publicKeyBuffer = new Buffer(publicKey, "base64");
      const signatureBuffer = new Buffer(signature, "base64");
      const hashBuffer = new Buffer(hash, "hex");

      const keys = ECPair.fromPublicKeyBuffer(publicKeyBuffer, undefined);
      const ecSignature = ECSignature.fromDER(signatureBuffer);
      const result = keys.verify(hashBuffer, ecSignature);
      return result;
    } catch (err) {
      return false;
    }
  }

  getHash(data: string): string {
    const array = this.encoder.encode(data);
    const hash = sha256(array);
    return this.getHexString(hash);
  }

  getHexArray(hex: string): Uint8Array {
    const array = [];
    const substrings = hex.match(/.{1,2}/g);
    for (const part of substrings) {
      array.push(parseInt(part, 16));
    }
    return Uint8Array.from(array);
  }

  getHexString(array: Uint8Array): string {
    let str = "";
    for (let i = 0; i < array.length; i++) {
      let hex = array[i].toString(16);
      if (hex.length % 2 !== 0) {
        hex = "0" + hex;
      }
      str += hex;
    }
    return str;
  }

  mineNonce(data: string, startingNonce: number = undefined): number {
    let nonce = startingNonce || 0;
    nonce--;
    let hash: Uint8Array;
    do {
      nonce++;
      hash = sha256(this.encoder.encode(nonce + data));
    } while (!(this.isSatisfyingDifficulty(hash)));

    return nonce;
  }

  private isSatisfyingDifficulty(array: Uint8Array): boolean {

    const leadingZeros = this.settings.difficulty;
    const fullZeros = Math.floor(leadingZeros / 2);

    for (let i = 0; i < fullZeros; i++) {
      if (array[i] !== 0) {
        return false;
      }
    }

    if (leadingZeros % 2 === 1) {
      // tslint:disable-next-line:no-bitwise
      if ((array[fullZeros] & 0xf0) !== 0) {
        return false;
      }
    }

    return true;
  }
}
