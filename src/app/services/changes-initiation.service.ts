import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs/Rx";



@Injectable()
export class ChangesInitiationService {

    private _changeInitiationIsNecessary: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public changeInitiationIsNecessary: Observable<boolean> = this._changeInitiationIsNecessary.asObservable();

    public initiateUpdate() {
        this._changeInitiationIsNecessary.next(true);
        setTimeout(() => this._changeInitiationIsNecessary.next(false), 100);
    }

}