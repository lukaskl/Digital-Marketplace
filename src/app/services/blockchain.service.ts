import { Injectable } from "@angular/core";
import { BlockContext } from "app/event-sourcing/contexts/block-context";
import { Block } from "app/models/block";
import { AccountsService } from "app/services/accounts.service";
import { BlocksService } from "app/services/blocks.service";
import { CryptoService } from "app/services/crypto.service";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { EventsService } from "app/services/events.service";
import { BlockValidator, BlockValidatorBase, GenesisBlockValidator } from "app/services/validation/block-validator";
import { Settings } from "app/settings";

@Injectable()
export class BlockchainService {

  private blocks: BlockContext[] = [];

  constructor(
    private settings: Settings,
    private blocksService: BlocksService,
    private crypto: CryptoService,
    private accounts: AccountsService,
    private events: EventsService,
    private eventHandlers: EventHandlersServices) {
  }

  async getBlock(blockHash: string): Promise<BlockContext> {
    return (await this.getBlocks())
      .find(block => block.block.hash === blockHash);
  }

  async getBlocks(): Promise<BlockContext[]> {
    // if (this.blocks === undefined) {
    //   this.blocks = await this.constructBlockchain();
    // }
    return this.blocks;
  }

  // TODO: make it run every two minutes
  // TODO: move to other class: block miner and add possibility to choose which events to mine, which to leave in the memPool
  async mineNewBlock(startingNonce: number = undefined, date: Date = undefined): Promise<BlockContext> {
    const newBlock = await this.blocksService.mineNewBlock(startingNonce, date);

    const blocks = await this.getBlocks();
    const lastBlock = blocks[blocks.length - 1];
    const validatorFactory = this.createValidator(newBlock, lastBlock);
    const blockContainer = new BlockContext(newBlock, validatorFactory, this, this.events, this.crypto);
    this.blocks.push(blockContainer);
    return blockContainer;
  }

  private createValidator(blockData: Block, previous: BlockContext = undefined): (BlockContext) => BlockValidatorBase {
    return previous === undefined ?
      (context: BlockContext) => new GenesisBlockValidator(context, this.crypto, this.settings, this.eventHandlers)
      : (context: BlockContext) => new BlockValidator(context, this.crypto, this.settings, this.eventHandlers);
  }
}
