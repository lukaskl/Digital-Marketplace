import { Injectable } from "@angular/core";
import { CoinbaseCreated } from "app/event-sourcing/events/money-transferred";
import { Block } from "app/models/block";
import { CryptoService } from "app/services/crypto.service";
import { EventsService } from "app/services/events.service";
import { Settings } from "app/settings";


@Injectable()
export class BlocksService {

  private blocks: Block[] = [];

  constructor(
    private settings: Settings,
    private eventsService: EventsService,
    private crypto: CryptoService) {

  }

  async getBlock(blockHash: string): Promise<Block> {
    return (await this.getBlocks())
      .find(block => block.hash === blockHash);
  }

  private sleep(ms: number): Promise<{}> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async getBlocks(): Promise<Block[]> {
    // if (this.blocks === undefined) {

    //   // just for demo purposes, to simulate database call
    //   await this.sleep(2000);
    //   this.blocks = BLOCKS;
    // }
    return this.blocks;
  }

  async mineNewBlock(startingNonce: number = undefined, date: Date = undefined): Promise<Block> {

    let events = await this.eventsService.getValidUnconfirmedEvents();
    events = events.filter(x => x.event.type !== CoinbaseCreated.TypeName);

    const coinbaseEvent = this.eventsService.createCoinbaseEvent(
      this.settings.coinbaseAddress,
      this.settings.coinbaseAmount,
      date);

    events.push(coinbaseEvent);

    const previousBlock = this.blocks[this.blocks.length - 1];
    let height: number;
    let previousHash: string;
    if (previousBlock) {
      height = previousBlock.height + 1;
      previousHash = previousBlock.hash;
    } else {
      height = 0;
      previousHash = this.settings.genesisBlockPreviousHash;
    }

    const block = new Block("", height, date || new Date(), 0, previousHash, events.map(x => x.event.hash));

    const nonce = this.crypto.mineNonce(block.getDataWithoutNonce(), startingNonce);
    block.nonce = nonce;

    const hash = this.crypto.getHash(block.getData());
    block.hash = hash;

    await this.eventsService.publishEvent(coinbaseEvent);

    this.blocks.push(block);

    this.eventsService.assignToBlock(events, block);

    return block;
  }

}