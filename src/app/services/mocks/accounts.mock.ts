
// TODO: remove these accounts
// infer them from the events

import { AccountSecret } from "app/models/account.secret";

export const ACCOUNT_SECRETS: AccountSecret[] = [
  new AccountSecret("Carrier", "A346Ls/3sAFayQTdV9E0FvvPVDAvDwVNmbJzBLEal3+3", "KwZTA1PW9iUCGCCeLFkx8UQfVQfwTGSjVfVqyh5qM7YspuUnJxA1"),
  new AccountSecret("Customer", "Agw67Iq/0QxbOU5POQnn6UkKGdE258o3eZEU/C43i2aT", "L1bLrCJi8ogxaFXw8q2s1azLJDu64GpNitJbH8X1vqMvttJYAFpk"),
  new AccountSecret("3rd party", "A22Om3dDLPk+vVnSXkf/tS8HSBblBXcZhywOef6lMza/", "L3CgmwnuvBhsQ6xKEu6UCxT4SiC4Go1rDBjLurBmiooQFzGJY8uk"),
];