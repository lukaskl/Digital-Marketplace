import { Injectable } from "@angular/core";
import { CryptoService } from "app/services/crypto.service";
import { Settings } from "app/settings";
import { JSEncrypt } from "jsencrypt";

import { AccountSecret } from "../models/account.secret";
import { ACCOUNT_SECRETS } from "app/services/mocks/accounts.mock";


@Injectable()
export class AccountSecretsService {

    private accounts: AccountSecret[];
    private rsaKeys: JSEncrypt[] = [];

    constructor(
        private settings: Settings,
        private crypto: CryptoService) {

        if (this.accounts === undefined) {
            this.accounts = ACCOUNT_SECRETS;
        }
    }

    async getAccountSecrets(): Promise<AccountSecret[]> {
        return this.accounts;
    }

    async findAccountSecret(publicAddress: string): Promise<AccountSecret> {
        return (await this.getAccountSecrets())
            .find(acc => acc.publicAddress === publicAddress);
    }

    async getAccountSecret(publicAddress: string): Promise<AccountSecret> {
        const acc = await this.findAccountSecret(publicAddress);

        if (!acc) {
            throw new Error("could not find acc '" + publicAddress + "'");
        }

        return acc;
    }

    getRsaKeys(): JSEncrypt[] {
        return this.rsaKeys;
    }

    findRsaKey(publicKey): JSEncrypt {
        return this.getRsaKeys()
            .find(acc => acc.getPublicKeyB64() === publicKey);
    }

    getRsaKey(publicKey: string): JSEncrypt {
        const key = this.findRsaKey(publicKey);

        if (!key) {
            throw new Error("could not find RSA key: '" + publicKey + "'");
        }

        return key;
    }

    async getAccountSslPrivateKey(publicAddress: string, publicRsaKey: string): Promise<JSEncrypt> {
        const acc = await this.getAccountSecret(publicAddress);
        const isOwner = acc.isOwnerOfKey(publicRsaKey);
        const rsaKey = this.rsaKeys.find(x => x.getPublicKeyB64() === publicRsaKey);

        if (!isOwner || !rsaKey) {
            throw new Error("The key '" + publicRsaKey + "' was not found or does not belong to the '"
                + publicAddress + "' (" + acc.displayName + ")")
        }

        return rsaKey;
    }

    async isOwnerOfRsaKey(publicAddress: string, publicRsaKey: string): Promise<boolean> {
        const acc = await this.findAccountSecret(publicAddress);
        if (!acc) {
            return false;
        }
        return acc.isOwnerOfKey(publicRsaKey);
    }

    async addNewAccount(displayName?: string): Promise<AccountSecret> {
        const acc = await this.crypto.generateAddress(displayName);
        this.accounts.push(acc);
        return acc;
    }

    generateNewRsaKey(): JSEncrypt {
        const rsaKey = this.crypto.generateRsaKey();
        this.rsaKeys.push(rsaKey);
        return rsaKey;
    }

    async assignKeyToAcc(publicRsaKey: string, accPublicAddress: string) {
        const acc = await this.getAccountSecret(accPublicAddress);
        acc.assignKey(publicRsaKey);
    }


}

