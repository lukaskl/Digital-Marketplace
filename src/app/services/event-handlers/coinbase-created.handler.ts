import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { CoinbaseCreated } from "app/event-sourcing/events/money-transferred";
import { AccountsService } from "app/services/accounts.service";
import { CryptoService } from "app/services/crypto.service";
import { EventHandler } from "app/services/event-handlers/event-handler";
import { Utils } from "utils";


@Injectable()
export class CoinbaseCreatedHandler extends EventHandler<CoinbaseCreated> {

  constructor(
    protected crypto: CryptoService,
    private accounts: AccountsService) {
    super(CoinbaseCreated.TypeName, crypto);
  }

   async applyImpl(event: EventContext<CoinbaseCreated>): Promise<void> {
     await this.accounts.createMoney(event.event.payload.to, event.event.payload.amount);
  }

  async getValidationErrors(event: EventContext<CoinbaseCreated>): Promise<string[]> {
    const errors =  await super.getValidationErrors(event);

    if (event.event.payload.amount < 0) {
      errors.push("Amount cannot be lower than zero");
    }

    if (Utils.isEmptyOrSpaces(event.event.payload.to)) {
      errors.push("To address mus be non empty");
    }

    return errors;
  }


}
