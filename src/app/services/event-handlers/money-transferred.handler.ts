import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { MoneyTransferred } from "app/event-sourcing/events/money-transferred";
import { BalanceAccount } from "app/models/balance-account";
import { AccountsService } from "app/services/accounts.service";
import { CryptoService } from "app/services/crypto.service";
import { SignableEventHandler } from "app/services/event-handlers/signable-event-handler";
import { Utils } from "utils";


@Injectable()
export class MoneyTransferredHandler extends SignableEventHandler<MoneyTransferred> {

  constructor(
    protected crypto: CryptoService,
    private accounts: AccountsService) {
    super(MoneyTransferred.TypeName, crypto);
  }

  async applyImpl(event: EventContext<MoneyTransferred>): Promise<void> {
    await this.accounts.transferMoney(event.event.payload.from, event.event.payload.to, event.event.payload.amount);
  }

  public async getValidationErrors(event: EventContext<MoneyTransferred>): Promise<string[]> {
    const errors = await super.getValidationErrors(event);

    if (event.event.payload.amount < 0) {
      errors.push("Amount cannot be lower than zero");
    }

    if (event.event.payload.transactionFee < 0) {
      errors.push("Transaction fee cannot be lower than zero");
    }

    if (Utils.isEmptyOrSpaces(event.event.payload.from)) {
      errors.push("From address mus be non empty");
    }

    if (Utils.isEmptyOrSpaces(event.event.payload.to)) {
      errors.push("To address mus be non empty");
    }

    let account: BalanceAccount;
    try {
      account = await this.accounts.getAccount(event.event.payload.from);

      if (account.balance < event.event.payload.amount + event.event.payload.transactionFee) {
        errors.push("Insufficient funds");
      }
    } catch (err) {
      errors.push("From account is non existing");
    }
    return errors;
  }

  public async getSignaturePublicAddress(event: EventContext<MoneyTransferred>): Promise<string> {
    return event.event.payload.from;
  }
}