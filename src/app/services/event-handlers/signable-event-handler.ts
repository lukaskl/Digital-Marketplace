import { EventContext } from "app/event-sourcing/contexts/event-context";
import { ISignableEvent } from "app/event-sourcing/events/event";
import { CryptoService } from "app/services/crypto.service";
import { EventHandler } from "app/services/event-handlers/event-handler";
import { Utils } from "utils";


export abstract class SignableEventHandler<TEvent extends ISignableEvent<any>>
  extends EventHandler<TEvent> {


  constructor(eventType: string, crypto: CryptoService) {
    super(eventType, crypto);
  }

  public async isSignatureValid(event: EventContext<TEvent>): Promise<boolean> {
    let publicAddress: string;
    try {
      publicAddress = await this.getSignaturePublicAddress(event);
    } catch (err) {
      return false;
    }
    return this.crypto.verify(event.event.hash, publicAddress, event.event.signature);
  }

  getSignature(event: TEvent, secret: string) {
    return this.crypto.sign(event.hash, secret);
  }

  public abstract getSignaturePublicAddress(event: EventContext<TEvent>): Promise<string>;

  public async getValidationErrors(event: EventContext<TEvent>): Promise<string[]> {
    const errors = await super.getValidationErrors(event);

    if (Utils.isEmptyOrSpaces(event.event.signature)) {
      errors.push("Signature is missing");
    } else {
      if (await this.isSignatureValid(event) === false) {
        errors.push("Signature is invalid");
      }
    }

    return errors;
  }

}
