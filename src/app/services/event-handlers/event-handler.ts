import { EventContext } from "app/event-sourcing/contexts/event-context";
import { IEvent } from "app/event-sourcing/events/event";
import { CryptoService } from "app/services/crypto.service";

export abstract class EventHandler<TEvent extends IEvent<any>> {

  constructor(
    public readonly eventType: string,
    protected crypto: CryptoService) {
  }

  async apply(event: EventContext<TEvent>): Promise<void> {
    const validationErrs = await this.getValidationErrors(event);

    if (validationErrs.length !== 0) {
      throw validationErrs;
    }

    await this.applyImpl(event);
  }

  protected abstract applyImpl(event: EventContext<TEvent>): Promise<void>;

  getHash(event: TEvent): string {
    const data = JSON.stringify({
      type: event.type,
      createdOn: event.createdOn.getTime(),
      payload: event.payload
    });
    return this.crypto.getHash(data);
  }

  async isValid(event: EventContext<TEvent>): Promise<boolean> {
    const errors = await this.getValidationErrors(event);

    // TODO: check if it is unique
    return errors.length === 0;
  }

  isHashValid(event: TEvent): boolean {
    const actualHash = this.getHash(event);
    return actualHash === event.hash;
  }

  async getValidationErrors(event: EventContext<TEvent>): Promise<string[]> {
    const errors: string[] = [];

    if (!this.isHashValid(event.event)) {
      errors.push("Invalid event's hash");
    }

    if (event.event.createdOn > new Date()) {
      errors.push("Event can not occur in the future");
    }

    return errors;
  }
}
