import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { TicketStatus } from "app/models/flight/ticket-status";
import { CryptoService } from "app/services/crypto.service";
import { SignableEventHandler } from "app/services/event-handlers/signable-event-handler";

import { SecretTicketPurchasedPayload, TicketPurchased } from "../../../event-sourcing/events/ticket-purchased";
import { AccountsService } from "../../accounts.service";
import { TicketsService } from "../../tickets.service";
import { JSEncrypt } from "jsencrypt";


@Injectable()
export class TicketPurchasedHandler extends SignableEventHandler<TicketPurchased> {

  constructor(
    crypto: CryptoService,
    private accounts: AccountsService,
    private tickets: TicketsService
  ) {
    super(TicketPurchased.TypeName, crypto);
  }

  public async getSignaturePublicAddress(event: EventContext<TicketPurchased>): Promise<string> {
    return event.event.payload.customerAccount;
  }

  protected async applyImpl(event: EventContext<TicketPurchased>): Promise<void> {
    const payload = event.event.payload;
    const ticket = await this.tickets.findTicket(payload.ticket);
    const account = await this.accounts.findAccount(payload.customerAccount);

    await this.accounts.transferMoney(account.publicAddress, ticket.carrierAccount, ticket.price);
    await this.tickets.purchaseTicket(event.event.payload.ticket, event.event.payload.customerAccount, payload.secretPayload);
  }

  public async getValidationErrors(event: EventContext<TicketPurchased>): Promise<string[]> {
    const errors = await super.getValidationErrors(event);

    const payload = event.event.payload;

    const ticket = await this.tickets.findTicket(payload.ticket);

    if (!ticket) {
      errors.push("Could not find ticket: '" + payload.ticket + "'");
    } else {
      if (
        !event.isPublished
        && ticket.state !== TicketStatus.Offered
        && ticket.state !== TicketStatus.Purchased) {
        errors.push("Ticket in state '" + TicketStatus[ticket.state] + "' can not be purchased");
      }
    }

    if (!payload.customerAccount) { errors.push("Customer's account is missing"); }
    if (!payload.secretPayload) { errors.push("Secret Payload is missing"); }

    if (!event.isPublished && payload.customerAccount && ticket) {
      const account = await this.accounts.findAccount(payload.customerAccount);
      if (account.balance < ticket.price) {
        errors.push("Insufficient funds");
      }
    }

    return errors;
  }

  public async getPrePublishValidationErrors(event: EventContext<TicketPurchased>,
    secretPayload: SecretTicketPurchasedPayload): Promise<string[]> {
    const errors: string[] = [];
    if (!secretPayload.customer) { errors.push("Customer's is missing"); }
    else {
      if (!secretPayload.customer.name) { errors.push("Customer's name is missing"); }
      if (!secretPayload.customer.surname) { errors.push("Customer's surname is missing"); }
    }

    if (!secretPayload.passenger) { errors.push("Passenger's info is missing"); }
    else {
      if (!secretPayload.passenger.name) { errors.push("Passenger's name is missing"); }
      if (!secretPayload.passenger.surname) { errors.push("Passenger's surname is missing"); }
      if (!secretPayload.passenger.age) { errors.push("Passenger's age is missing"); }
      if (!secretPayload.passenger.nationality) { errors.push("Passenger's nationality is missing"); }
      if (!secretPayload.passenger.passportId) { errors.push("Passenger's passport id is missing"); }
    }

    return errors;
  }

}
