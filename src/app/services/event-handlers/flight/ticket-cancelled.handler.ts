import { TicketCancelled } from "../../../event-sourcing/events/ticket-cancelled";
import { TicketsService } from "../../tickets.service";
import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { TicketStatus } from "app/models/flight/ticket-status";
import { CryptoService } from "app/services/crypto.service";
import { SignableEventHandler } from "app/services/event-handlers/signable-event-handler";


@Injectable()
export class TicketCancelledHandler extends SignableEventHandler<TicketCancelled> {

  constructor(
    crypto: CryptoService,
    private tickets: TicketsService
  ) {
    super(TicketCancelled.TypeName, crypto);
  }

  public async getSignaturePublicAddress(event: EventContext<TicketCancelled>): Promise<string> {
    const ticket = await this.tickets.getTicket(event.event.payload.ticket);

    return ticket.carrierAccount;
  }

  protected applyImpl(event: EventContext<TicketCancelled>): Promise<void> {
    return this.tickets.cancelTicket(event.event.payload.ticket);
  }

  public async getValidationErrors(event: EventContext<TicketCancelled>) {
    const errors = await super.getValidationErrors(event);

    const payload = event.event.payload;

    const ticket = await this.tickets.findTicket(payload.ticket);

    if (!ticket) {
      errors.push("Could not find ticket: '" + payload.ticket + "'");
    } else {

      if (!event.isPublished
        && ticket.state !== TicketStatus.Offered
        && ticket.state !== TicketStatus.Purchased) {
        errors.push("Ticket in state '" + TicketStatus[ticket.state] + "' can not be cancelled");
      }

    }

    return errors;
  }

}
