import { AccountSecretsService } from "../../accountSecrets.service";
import { TicketsService } from "../../tickets.service";
import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { TicketAnnounced } from "app/event-sourcing/events/ticket-announced";
import { CryptoService } from "app/services/crypto.service";
import { SignableEventHandler } from "app/services/event-handlers/signable-event-handler";


@Injectable()
export class TicketAnnouncedHandler extends SignableEventHandler<TicketAnnounced> {

  constructor(
    crypto: CryptoService,
    private accountSecrets: AccountSecretsService,
    private tickets: TicketsService) {
    super(TicketAnnounced.TypeName, crypto);
  }

  public async getSignaturePublicAddress(event: EventContext<TicketAnnounced>): Promise<string> {
    return event.event.payload.carrierAccount;
  }

  protected applyImpl(event: EventContext<TicketAnnounced>): Promise<void> {

    this.accountSecrets.assignKeyToAcc(event.event.payload.publicRsaKey, event.event.payload.carrierAccount);
    return this.tickets.announceTicket(event.event.hash, event.event.payload);
  }

  public async getValidationErrors(event: EventContext<TicketAnnounced>) {
    const errors = await super.getValidationErrors(event);

    const payload = event.event.payload;

    if (!payload.arrivalTime) { errors.push("Arrival time is missing"); }
    if (!payload.departureTime) { errors.push("Departure time is missing"); }
    if (!payload.arrival) { errors.push("Arrival location is missing"); }
    if (!payload.departure) { errors.push("Departure location is missing"); }
    if (!payload.carrierAccount) { errors.push("Carrier account is missing"); }
    if (!payload.flightNumber) { errors.push("Flight number is missing"); }
    if (!payload.seat) { errors.push("Seat number is missing"); }
    if (!payload.price) { errors.push("Price is missing"); }

    if (payload.arrivalTime && payload.departureTime
      && payload.arrivalTime < payload.departureTime) {
      errors.push("Departure time cannot be later than arrival time");
    }

    if (payload.price < 0) {
      errors.push("Price cannot be negative");
    }

    return errors;
  }

}
