import { Injectable } from "@angular/core";
import { TicketAnnouncedPayload } from "app/event-sourcing/events/ticket-announced";

import { Customer } from "../models/flight/customer";
import { Passenger } from "../models/flight/passenger";
import { Ticket } from "../models/flight/ticket";
import { TicketStatus } from "../models/flight/ticket-status";

@Injectable()
export class TicketsService {

  private tickets: Ticket[] = [];

  public getTickets(): Promise<Ticket[]> {
    return Promise.resolve(this.tickets);
  }

  public async getTicket(hash: string): Promise<Ticket> {
    const result = await this.findTicket(hash);

    if (!result) {
      throw new Error("could not find ticket: '" + hash + "'");
    }

    return result;
  }

  public async findTicket(hash: string): Promise<Ticket> {
    return this.tickets.find(x => x.hashId === hash);
  }

  public async announceTicket(hash: string, payload: TicketAnnouncedPayload): Promise<void> {

    if (await this.findTicket(hash)) {
      throw new Error("Ticket already exists");
    }

    const ticket: Ticket = {
      arrival: payload.arrival,
      arrivalTime: payload.arrivalTime,
      departure: payload.departure,
      departureTime: payload.departureTime,
      carrierAccount: payload.carrierAccount,
      carrierPublicRsaKey: payload.publicRsaKey,
      price: payload.price,
      seat: payload.seat,
      hashId: hash,
      customerAccount: undefined,
      secretPayload: undefined,
      state: TicketStatus.Offered
    };

    this.tickets.push(ticket);
  }


  public async purchaseTicket(hashId: string, customerAccount: string, secretPayload: string): Promise<void> {
    if (!customerAccount) {
      throw new Error("Missing property: customer account");
    }

    if (!secretPayload) {
      throw new Error("Missing property: passenger");
    }

    const ticket = await this.getTicket(hashId);

    if (ticket.state !== TicketStatus.Offered) {
      throw new Error("Ticket in state '" + ticket.state + "' can not be purchased");
    }

    ticket.customerAccount = customerAccount;
    ticket.secretPayload = secretPayload;
    ticket.state = TicketStatus.Purchased;
  }

  public async cancelTicket(hashId: string): Promise<void> {
    const ticket = await this.getTicket(hashId);

    if ((ticket.state !== TicketStatus.Offered)
      && (ticket.state !== TicketStatus.Purchased)) {
      throw new Error("Ticket in state '" + ticket.state + "' can not be cancelled");
    }

    ticket.state = TicketStatus.Canceled;
  }

  public async conductTicket(hashId: string): Promise<void> {
    const ticket = await this.getTicket(hashId);

    if (ticket.state !== TicketStatus.Purchased) {
      throw new Error("Ticket in state '" + ticket.state + "' can not be conducted");
    }

    ticket.state = TicketStatus.Conducted;
  }

}
