import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { MoneyTransferred } from "app/event-sourcing/events/money-transferred";
import { AccountSecret } from "app/models/account.secret";
import { BlockchainService } from "app/services/blockchain.service";
import { CryptoService } from "app/services/crypto.service";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { MoneyTransferredHandler } from "app/services/event-handlers/money-transferred.handler";
import { EventsService } from "app/services/events.service";
import { ACCOUNT_SECRETS } from "app/services/mocks/accounts.mock";

import { TicketAnnounced } from "../event-sourcing/events/ticket-announced";
import { SecretTicketPurchasedPayload, TicketPurchased } from "../event-sourcing/events/ticket-purchased";
import { AccountSecretsService } from "./accountSecrets.service";
import { TicketAnnouncedHandler } from "./event-handlers/flight/ticket-announced.handler";
import { TicketPurchasedHandler } from "./event-handlers/flight/ticket-purchased.handler";
import { JSEncrypt } from "jsencrypt";


@Injectable()
export class InitializationService {

  constructor(
    private events: EventsService,
    private blocks: BlockchainService,
    private crypto: CryptoService,
    private eventHandlers: EventHandlersServices,
    private accountSecrets: AccountSecretsService

  ) { }


  async loadMocks(): Promise<void> {
    await this.blocks.mineNewBlock(68994, new Date("2017-05-27 19:49:13"));

    await this.events.publishEvent(this.creatTransferMoneyEvent(ACCOUNT_SECRETS[0], ACCOUNT_SECRETS[1], new Date("2017-05-27 20:47:13")));
    await this.events.publishEvent(this.creatTransferMoneyEvent(ACCOUNT_SECRETS[0], ACCOUNT_SECRETS[2], new Date("2017-05-27 20:46:13")));

    await this.blocks.mineNewBlock(28629, new Date("2017-05-27 20:49:13"));
    await this.blocks.mineNewBlock(36533, new Date("2017-05-27 21:49:13"));
    await this.blocks.mineNewBlock(432374, new Date("2017-05-27 21:50:13"));

    const announcedTicket = this.createTicketAnnouncementEvent(ACCOUNT_SECRETS[0]);
    await this.events.publishEvent(announcedTicket);
    await this.events.publishEvent(this.createTicketPurchasedEvent(announcedTicket.event.hash,
     announcedTicket.event.payload.publicRsaKey, ACCOUNT_SECRETS[1]));
  }


  creatTransferMoneyEvent(from: AccountSecret, to: AccountSecret, date: Date = undefined): EventContext<MoneyTransferred> {
    const event = new MoneyTransferred({
      from: from.publicAddress,
      to: to.publicAddress,
      transactionFee: 0.02,
      amount: 20
    }, date || new Date(), "", "");

    const handler = this.eventHandlers.getEventHandler(event) as MoneyTransferredHandler;

    event.hash = handler.getHash(event);
    event.signature = handler.getSignature(event, from.privateKeyWIF);

    const result = new EventContext<MoneyTransferred>({ event: event });
    return result;
  }

  createTicketAnnouncementEvent(carrierAcc: AccountSecret): EventContext<TicketAnnounced> {

    const rsaKey = this.accountSecrets.generateNewRsaKey();
    const event = new TicketAnnounced({
      departure: "AMS",
      departureTime: new Date("2018-04-30T18:10:00.000Z"),
      arrival: "ARN",
      arrivalTime: new Date("2018-04-30T20:55:27.000Z"),
      carrierAccount: carrierAcc.publicAddress,
      flightNumber: "KLM 1121",
      price: 15,
      seat: "16E",
      publicRsaKey: rsaKey.getPublicKeyB64()
    }, new Date(), "", "");

    const handler = this.eventHandlers.getEventHandler(event) as TicketAnnouncedHandler;

    event.hash = handler.getHash(event);
    event.signature = handler.getSignature(event, carrierAcc.privateKeyWIF);

    const result = new EventContext<TicketAnnounced>({ event: event });

    return result;
  }

  createTicketPurchasedEvent(ticket: string, carrierRsaKey: string, customerAcc: AccountSecret): EventContext<TicketPurchased> {

    const secretPayload: SecretTicketPurchasedPayload = {
      customer: {
        name: "Luis",
        surname: "Armstrong"
      },
      passenger: {
        name: "Nina",
        age: 35,
        nationality: "Dutch",
        surname: "Armstrong",
        passportId: "123-KN-75"
      }
    };

    const crypt = new JSEncrypt();
    crypt.setKey(carrierRsaKey);

    const event = new TicketPurchased({
      ticket: ticket,
      customerAccount: customerAcc.publicAddress,
      secretPayload: crypt.encrypt(JSON.stringify(secretPayload))
    }, new Date(), "", "");

    const handler = this.eventHandlers.getEventHandler(event) as TicketPurchasedHandler;

    event.hash = handler.getHash(event);
    event.signature = handler.getSignature(event, customerAcc.privateKeyWIF);

    const result = new EventContext<TicketPurchased>({ event: event });

    return result;
  }

}
