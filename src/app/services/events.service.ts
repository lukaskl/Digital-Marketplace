import { Injectable } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { IEvent } from "app/event-sourcing/events/event";
import { CoinbaseCreated } from "app/event-sourcing/events/money-transferred";
import { Block } from "app/models/block";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { EventHandler } from "app/services/event-handlers/event-handler";

@Injectable()
export class EventsService {

  events: EventContext<IEvent<any>>[] = [];


  constructor(private eventHandlers: EventHandlersServices) {

  }

  async getEvent(hashId: string): Promise<EventContext<IEvent<any>>> {
    const events = await this.getEvents();
    const result = events
      .find(x => x.event.hash === hashId);

    if (result === undefined) {
      throw new Error("could not find event with hash '" + hashId + ";");
    }

    return result;
  }

  async publishEvent(event: EventContext<IEvent<any>>): Promise<void> {

    if (event.isPublished) {
      throw new Error("Event was already published");
    }

    event.isPublished = true;

    let eventHandler: EventHandler<IEvent<any>>;
    try {
      eventHandler = this.eventHandlers.getEventHandler(event.event);
    } catch (err) {
      console.error("Could not get an event handler for event of type '" + event.event.type + "'");
      throw err;
    }

    try {
      await eventHandler.apply(event);
    } catch (err) {
      console.error("Could not publish event: '" + JSON.stringify(event.event) + "' reason: " + err);
      throw err;
    }

    this.events.push(event);
  }

  async getValidUnconfirmedEvents(): Promise<EventContext<IEvent<any>>[]> {
    const result = [];
    for (const event of await this.getEvents()) {
      if (event.isConfirmed === true) { continue; }

      try {
        const eventHandler = this.eventHandlers.getEventHandler(event.event);

        // TODO: check if we are not taking valid events which are dependent on invalid events;
        if (await eventHandler.isValid(event) === true) {
          result.push(event);
        }
      } catch (err) {/* ignore - event is not valid*/ }
    }
    return result;
  }

  createCoinbaseEvent(to: string, amount: number, date: Date = undefined): EventContext<IEvent<any>> {
    const eventData = new CoinbaseCreated({ to: to, amount: amount }, date || new Date(), "");
    const event = new EventContext<CoinbaseCreated>({ event: eventData });
    const handler = this.eventHandlers.getEventHandler(event.event);
    event.event.hash = handler.getHash(event.event);
    return event;
  }

  async assignToBlock(events: EventContext<IEvent<any>>[], block: Block): Promise<void> {
    for (const event of events) {
      event.confirmedInBlock = block;
    }
  }

  async getEvents(): Promise<EventContext<IEvent<any>>[]> {
    return this.events;
  }

}
