import { Injectable } from "@angular/core";
import { IEvent } from "app/event-sourcing/events/event";
import { Block } from "app/models/block";
import { CryptoService } from "app/services/crypto.service";
import { Settings } from "app/settings";
import { BlockContext } from "app/event-sourcing/contexts/block-context";
import { EventHandlersServices } from "app/services/event-handlers.service";

export abstract class BlockValidatorBase {

  constructor(
    protected context: BlockContext,
    protected cryptoService: CryptoService,
    protected settings: Settings,
    protected eventHandlers: EventHandlersServices) {

  }

  async isValid(): Promise<boolean> {
    const errors = await this.getValidationErrors();
    if (errors.length === 0) {
      return true;
    }
    return false;
  }

  abstract getPreviousBlockErrors(): Promise<string[]>;

  abstract isHeightValid(): Promise<boolean>;

  async isBlockHashValid(): Promise<boolean> {

    if (this.context.calculateHash() !== this.context.block.hash) {
      return false;
    }

    if (this.context.block.hash.substr(0, this.settings.difficulty) !== Array(this.settings.difficulty + 1).join("0")) {
      return false;
    }
  }

  async getInvalidEvents(): Promise<string[]> {
    const events = await this.context.getEvents();

    const result: string[] = [];
    for (const event of events.loadedElements) {
      const handler = this.eventHandlers.getEventHandler(event.event);
      if (await handler.isValid(event) === false) {
        result.push("Invalid event: '" + event.event.hash + "'");
      }
    }

    for (const eventHash of events.invalidHashes) {
      result.push("Non existing event: '" + eventHash + "'");
    }

    return result;
  }

  async getValidationErrors(): Promise<string[]> {
    const errors: string[] = [];

    for (const err of await this.getPreviousBlockErrors()) {
      errors.push(err);
    }

    if (await this.isBlockHashValid() === false) {
      errors.push("Block's hash is not valid");
    }

    if (await this.isHeightValid() === false) {
      errors.push("Block's height is not valid");
    }

    for (const err of await this.getInvalidEvents()) {
      errors.push(err);
    }

    return errors;
  }
}

export class GenesisBlockValidator extends BlockValidatorBase {

  constructor(
    context: BlockContext,
    cryptoService: CryptoService,
    settings: Settings,
    eventHandlers: EventHandlersServices) {
    super(context, cryptoService, settings, eventHandlers);
  }

  async getPreviousBlockErrors(): Promise<string[]> {
    if (this.context.block.previousHash !== this.settings.genesisBlockPreviousHash) {
      return ["Previous block's hash is not a zero hash"];
    }

    return [];
  }



  async isHeightValid(): Promise<boolean> {
    return this.context.block.height === 0;
  }
}

export class BlockValidator extends BlockValidatorBase {

  constructor(
    block: BlockContext,
    cryptoService: CryptoService,
    settings: Settings,
    eventHandlers: EventHandlersServices) {
    super(block, cryptoService, settings, eventHandlers);
  }


  async getPreviousBlockErrors(): Promise<string[]> {
    const result: string[] = [];

    const prevBlock = await this.context.getPreviousBlock();

    if (!prevBlock) {
      result.push("Previous block was not found");
    } else {
      const errors = await prevBlock.validator.getValidationErrors();

      if (errors.length !== 0) {
        result.push("Previous block is invalid");
      }
    }
    return result;
  }


  async isHeightValid(): Promise<boolean> {

    const prevBlock = await this.context.getPreviousBlock();

    if (prevBlock) {
      return prevBlock.block.height + 1 === this.context.block.height;
    }
  }
}
