import { BalanceAccount } from "app/models/balance-account";
import { Injectable } from "@angular/core";


@Injectable()
export class AccountsService {

  private accounts: BalanceAccount[] = [];

  async createMoney(account: string, amount: number): Promise<void> {
    if (amount < 0) {
      throw new Error("Can not create new account with a negative (" + amount + ") balance");
    }
    try {
      const acc = await this.getAccount(account);
      acc.balance += amount;
    } catch (err) {
      await this.addAccount(account, amount);
    }
  }

  async getAccounts(): Promise<BalanceAccount[]> {
    return this.accounts;
  }

  async getAccount(publicAddress: string): Promise<BalanceAccount> {
    const account = await this.findAccount(publicAddress);
    if (!account) {
      throw new Error("Could not find account: '" + publicAddress + "'");
    }
    return account;
  }

  async findAccount(publicAddress: string): Promise<BalanceAccount> {
    return this.accounts.find(acc => acc.publicAddress === publicAddress);
  }


  addAccount(publicAddress: string, balance: number): Promise<BalanceAccount> {

    if (balance < 0) {
      throw new Error("Account balance can not be negative");
    }
    const account = new BalanceAccount(publicAddress, balance);
    this.accounts.push(account);

    return new Promise<BalanceAccount>((resolve, reject) => resolve(account));
  }

  async transferMoney(from: string, to: string, amount: number) {
    const fromAccount = await this.getAccount(from);

    let toAccount: BalanceAccount;
    try {
      toAccount = await this.getAccount(to);
    } catch (err) {
      toAccount = await this.addAccount(to, 0);
    }

    if (fromAccount.balance < amount) {
      throw new Error("Insufficient funds. There is not enough money on account '" + fromAccount.publicAddress + "'");
    }

    fromAccount.balance -= amount;
    toAccount.balance += amount;
  }

}
