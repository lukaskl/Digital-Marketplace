import { AccountSecretsService } from "../../services/accountSecrets.service";

import { Component, DoCheck, EventEmitter, Output } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { CoinbaseCreated, MoneyTransferred } from "app/event-sourcing/events/money-transferred";
import { TicketAnnounced } from "app/event-sourcing/events/ticket-announced";
import { Customer } from "app/models/flight/customer";
import { Passenger } from "app/models/flight/passenger";
import { TicketPurchased } from "app/event-sourcing/events/ticket-purchased";
import { TicketCancelled } from "app/event-sourcing/events/ticket-cancelled";
import { IEvent } from "app/event-sourcing/events/event";


@Component({
    selector: "app-add-new-event",
    templateUrl: "./add-new.component.html",
    styleUrls: ["./add-new.component.scss"],
})
export class AddNewEventComponent {

    @Output() newEventAdded = new EventEmitter<EventContext<IEvent<any>>>();

    constructor(private accountSecrets: AccountSecretsService) {

    }

    addNewCoinbaseCreated() {
        const event = new EventContext<CoinbaseCreated>({
            event: new CoinbaseCreated({ amount: 20, to: "" }, new Date(), "")
        });

        this.newEventAdded.emit(event);
    }

    addNewMoneyTransferred() {
        const event = new EventContext<MoneyTransferred>({
            event: new MoneyTransferred({ from: "", to: "", amount: 10, transactionFee: 0.002 }, new Date(), "", "")
        });
        this.newEventAdded.emit(event);
    }

    addNewTicketAnnounced() {

        const key = this.accountSecrets.generateNewRsaKey();

        const event = new EventContext<TicketAnnounced>({
            event: new TicketAnnounced({
                arrival: "AMS",
                departure: "ARN",
                arrivalTime: new Date(),
                departureTime: new Date(),
                carrierAccount: "",
                flightNumber: "KLM 1121",
                price: 20,
                seat: "6B",
                publicRsaKey: key.getPublicKeyB64()

            }, new Date(), "", "")
        });

        event.event.payload.departureTime.setMonth(event.event.payload.departureTime.getMonth() + 10);
        event.event.payload.arrivalTime.setMonth(event.event.payload.arrivalTime.getMonth() + 10);

        event.event.payload.arrivalTime.setHours(event.event.payload.arrivalTime.getHours() + 2.1);

        this.newEventAdded.emit(event);
    }

    addNewTicketPurchased() {

        const event = new EventContext<TicketPurchased>({
            event: new TicketPurchased({
                customerAccount: "",
                secretPayload: "",
                ticket: ""
            }, new Date(), "", "")
        });

        this.newEventAdded.emit(event);
    }

    addNewTicketCancelled() {

        const event = new EventContext<TicketCancelled>({
            event: new TicketCancelled({
                ticket: "",
                reason: "some reason"
            }, new Date(), "", "")
        });

        this.newEventAdded.emit(event);
    }

}
