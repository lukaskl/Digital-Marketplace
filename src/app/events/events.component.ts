import { MoneyTransferred } from "../event-sourcing/events/money-transferred";
import { TicketCancelled } from "../event-sourcing/events/ticket-cancelled";
import { TicketAnnounced } from "../event-sourcing/events/ticket-announced";
import { TicketPurchased } from "../event-sourcing/events/ticket-purchased";
import { Customer } from "../models/flight/customer";
import { Passenger } from "../models/flight/passenger";
import { Component, OnInit } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { IEvent } from "app/event-sourcing/events/event";
import { CoinbaseCreated } from "app/event-sourcing/events/money-transferred";
import { EventsService } from "app/services/events.service";

@Component({
  selector: "app-events",
  templateUrl: "./events.component.html",
  styleUrls: ["./events.component.scss"],
})
export class EventsComponent implements OnInit {

  events: EventContext<IEvent<any>>[] = [];

  private _notPublishedEvents: EventContext<IEvent<any>>[] = [];

  get notPublishedEvents(): EventContext<IEvent<any>>[] {
    this._notPublishedEvents = this._notPublishedEvents.filter(x => !x.isPublished);
    return this._notPublishedEvents;
  }

  constructor(
    private eventsService: EventsService
  ) { }

  async ngOnInit() {
    this.events = await this.eventsService.getEvents();
  }


  addNewEvent(event: EventContext<IEvent<any>>) {
    if (event) {
      this._notPublishedEvents.push(event);
    }
  }
}
