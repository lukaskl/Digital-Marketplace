import { CoinbaseCreated } from "../../event-sourcing/events/money-transferred";
import { CoinbaseCreatedHandler } from "../../services/event-handlers/coinbase-created.handler";
import { Component, Input } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { EventHandlersServices } from "app/services/event-handlers.service";

@Component({
  selector: "app-coinbase-created",
  templateUrl: "./coinbase-created.component.html",
})
export class CoinbaseCreatedComponent {

  private _event: EventContext<CoinbaseCreated>;

  private handler: CoinbaseCreatedHandler;

  @Input()
  get event(): EventContext<CoinbaseCreated> {
    return this._event;
  }
  set event(event: EventContext<CoinbaseCreated>) {
    this._event = event;
    this.handler = this.eventHandlers.getEventHandler(this.event.event) as CoinbaseCreatedHandler;
  }

  constructor(
    private eventHandlers: EventHandlersServices,
  ) { }

}
