import { Component, Input } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { MoneyTransferred } from "app/event-sourcing/events/money-transferred";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { MoneyTransferredHandler } from "app/services/event-handlers/money-transferred.handler";

@Component({
  selector: "app-money-transferred",
  templateUrl: "./money-transferred.component.html",
  styles: ["./money-transferred.component.scss"],
})
export class MoneyTransferredComponent {

  private _event: EventContext<MoneyTransferred>;

  private handler: MoneyTransferredHandler;

  @Input()
  get event(): EventContext<MoneyTransferred> {
    return this._event;
  }
  set event(event: EventContext<MoneyTransferred>) {
    this._event = event;
    this.handler = this.eventHandlers.getEventHandler(this.event.event) as MoneyTransferredHandler;
  }

  constructor(
    private eventHandlers: EventHandlersServices,
  ) { }

}
