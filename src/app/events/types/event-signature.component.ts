import { SignableEventHandler } from "../../services/event-handlers/signable-event-handler";
import { ACCOUNT_SECRETS } from "../../services/mocks/accounts.mock";
import { OnInit } from "@angular/core/core";
import { ISignableEvent } from "../../event-sourcing/events/event";
import { Component, Input, DoCheck } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { MoneyTransferredHandler } from "app/services/event-handlers/money-transferred.handler";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { CryptoService } from "app/services/crypto.service";


// THIS WHOLE COMPONENT IS A LIE!!
// This component is meant to manage signature of the event
// the tricky part is that the secrets (private keys)
// indeed should be kept in secret.
//
// This component is able to access secrets only for demo
// and simplicity purposes

@Component({
  selector: "app-signable-event",
  templateUrl: "event-signature.component.html"
})
export class EventSignatureComponent implements DoCheck {

  private _event: EventContext<ISignableEvent<any>>;
  private handler: SignableEventHandler<ISignableEvent<any>>;

  private _eventCache: string;
  private _publicAddressCache: string;

  @Input()
  get event(): EventContext<ISignableEvent<any>> {
    return this._event;
  }
  set event(event: EventContext<ISignableEvent<any>>) {
    this._event = event;
    this.handler = this.eventHandlers.getEventHandler(this.event.event) as MoneyTransferredHandler;
  }

  canCalculateSignature: boolean;

  constructor(
    private eventHandlers: EventHandlersServices,
    private crypto: CryptoService
  ) { }

  async calculateSignature(): Promise<void> {
    try {

      this.event.event.hash = this.handler.getHash(this.event.event);

      const publicAddress = await this.handler.getSignaturePublicAddress(this.event);
      const secret = ACCOUNT_SECRETS.find(x => x.publicAddress === publicAddress);
      this.event.event.signature = this.handler.getSignature(this.event.event, secret.privateKeyWIF);
    } catch (err) {
      console.error(err);
    }
  }

  ngDoCheck() {
    const eventCache = JSON.stringify(this.event.event);
    if (this._eventCache !== eventCache) {
      this._eventCache = eventCache;

      this.checkCanCalculateSignature().then(x => {
        this.canCalculateSignature = x;
      });
    }
  }

  private async checkCanCalculateSignature(): Promise<boolean> {
    try {
      const publicAddress = await this.handler.getSignaturePublicAddress(this.event);
      this._publicAddressCache = publicAddress;
      const account = ACCOUNT_SECRETS.find(x => x.publicAddress === publicAddress);

      const isSignatureValid = await this.handler.isSignatureValid(this.event);

      if (isSignatureValid === false && account && account.privateKeyWIF) {
        return true;
      }
      return false;

    } catch (err) {
      return false;
    }
  }

}