import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { TicketPurchasedHandler } from "app/services/event-handlers/flight/ticket-purchased.handler";
import { JSEncrypt } from "jsencrypt";

import { SecretTicketPurchasedPayload, TicketPurchased } from "../../event-sourcing/events/ticket-purchased";
import { TicketsService } from "../../services/tickets.service";

@Component({
    selector: "app-ticket-purchased",
    templateUrl: "./ticket-purchased.component.html",
})
export class TicketPurchasedComponent implements DoCheck, OnInit {

    private _event: EventContext<TicketPurchased>;
    private secretPayload: SecretTicketPurchasedPayload;
    private _previousSecretPayloadState: string;
    private _previousTicket: string;

    private handler: TicketPurchasedHandler;

    @Output() prePublishValidationErrorsCounted = new EventEmitter<string[]>();

    @Input()
    get event(): EventContext<TicketPurchased> {
        return this._event;
    }
    set event(event: EventContext<TicketPurchased>) {
        this._event = event;
        this.handler = this.eventHandlers.getEventHandler(this.event.event) as TicketPurchasedHandler;
    }

    get secretPayloadSize() {
        return JSON.stringify(this.secretPayload).length;
    }

    async updateSecretPayload() {

        if (this.event.isPublished) {
            return;
        }

        const ticket = await this.tickets.findTicket(this.event.event.payload.ticket);
        if (!ticket) {
            this.event.event.payload.secretPayload = undefined;
            return;
        }

        const rsaKey = new JSEncrypt();
        rsaKey.setKey(ticket.carrierPublicRsaKey);

        this.event.event.payload.secretPayload = rsaKey.encrypt(JSON.stringify(this.secretPayload));
    }

    constructor(
        private eventHandlers: EventHandlersServices,
        private tickets: TicketsService
    ) { }


    ngOnInit() {
        if (!this.event.isPublished) {
            this.secretPayload = this.preFillSecretDetails();
        }
    }

    ngDoCheck() {

        if (this.event.isPublished) {
            return;
        }

        const currentState = JSON.stringify(this.secretPayload);
        if (this._previousSecretPayloadState !== currentState
            || this._previousTicket !== this.event.event.payload.ticket) {

            this.updateSecretPayload();

            this._previousSecretPayloadState = currentState;
            this._previousTicket = this.event.event.payload.ticket;

            this.handler.getPrePublishValidationErrors(this.event, this.secretPayload)
                .then(errs => this.prePublishValidationErrorsCounted.emit(errs));
        }
    }

    private preFillSecretDetails(): SecretTicketPurchasedPayload {
        const secretPayload = {
            customer: {
                name: "Mon",
                surname: "Mothma"
            },
            passenger: {
                age: 33,
                name: "Lyra",
                nationality: "Canadian",
                passportId: "3-168-7345",
                surname: "Erso"
            }
        };

        return secretPayload;
    }

}
