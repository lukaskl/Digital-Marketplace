import { Component, Input, OnInit, ViewEncapsulation, DoCheck, AfterContentChecked } from "@angular/core";

import { CryptoService } from "app/services/crypto.service";
import { IEvent } from "app/event-sourcing/events/event";
import { EventContext } from "app/event-sourcing/contexts/event-context";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { EventHandler } from "app/services/event-handlers/event-handler";
import { Utils } from "utils";
import { EventsService } from "app/services/events.service";


@Component({
  selector: "app-event",
  templateUrl: "./event.component.html",
  styleUrls: ["./event.component.scss"],
})
export class EventComponent implements DoCheck {


  private _event: EventContext<IEvent<any>>;
  private _previousEventState: string;
  private handler: EventHandler<IEvent<any>>;

  @Input()
  get event(): EventContext<IEvent<any>> {
    return this._event;
  }
  set event(event: EventContext<IEvent<any>>) {
    this._event = event;
    this.handler = this.eventHandlers.getEventHandler(this.event.event);
    this.ngDoCheck();
  }

  // states to represent async values

  errorsMessages: string[] = [];
  softErrorMessages: string[] = [];


  constructor(
    private eventHandlers: EventHandlersServices,
    private events: EventsService) {
  }

  get payload(): string {
    return JSON.stringify(this.event.event.payload, undefined, 2);
  }
  set payload(value: string) {
    try {
      this.event.event.payload = JSON.parse(value);
    } catch (err) {

    }
  }

  get isValid(): boolean {
    return this.errorsMessages.length === 0 && this.softErrorMessages.length === 0;
  }

  get status(): string {
    if (this.isValid !== true) {
      return "invalid";
    }
    if (this.event.isConfirmed === true) {
      return "confirmed";
    }
    if (!this.event.isConfirmed === true) {
      return "unconfirmed";
    }
    return "unknown";
  }

  calculateHash(): string {
    return this.handler.getHash(this.event.event);
  }

  isHashValid() {
    return this.handler.isHashValid(this.event.event);
  }

  copyCalculatedHash() {
    this.event.event.hash = this.handler.getHash(this.event.event);
  }

  async publishEvent(): Promise<void> {
    await this.events.publishEvent(this.event);
  }

  // -------------------------------------------------------------
  // -------------           async updates           -------------
  // -------------------------------------------------------------

  ngDoCheck(): void {
    const currentEventState = JSON.stringify(this.event);

    if (this._previousEventState !== currentEventState) {
      this._previousEventState = currentEventState;
      this.getErrorsMessages().then(msg => this.errorsMessages = msg);
    }
  }

  private getErrorsMessages(): Promise<string[]> {
    return this.handler.getValidationErrors(this.event);
  }

  handlePrePublishValidationErrors(errors: string[]) {
    this.softErrorMessages = errors;
  }
}
