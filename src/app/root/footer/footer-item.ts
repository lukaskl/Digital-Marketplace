

export class FooterItemButton<T> {

    constructor(
        public allValues: T[],
        public currentValue: (() => T),
        public displayNameSelector: ((value: T) => string),
        public onSelected: ((value: T) => void),
        public width: number,
        public iconKey: string,
        public toolTip: string = undefined,
        public initChangeDetectionAfterUpdate = false
    ) { }

}