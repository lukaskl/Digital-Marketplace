import { Component, OnInit } from "@angular/core";

import { AccountSecret } from "../../models/account.secret";
import { AccountSecretsService } from "../../services/accountSecrets.service";
import { Settings } from "../../settings";
import { FooterItemButton } from "./footer-item";

@Component({
    selector: "app-footer",
    templateUrl: "footer.component.html",
    styleUrls: ["footer.component.scss"]
})
export class FooterComponent implements OnInit {

    items: FooterItemButton<any>[] = [];

    constructor(private settings: Settings,
        private accountSecrets: AccountSecretsService) { }

    ngOnInit(): void {

        console.log("footer");
        this.getSettings().then(x => this.items = x);

    }

    ngDoCheck(){
        console.log("footer2");
    }

    async getSettings(): Promise<FooterItemButton<any>[]> {
        const difficulty = new FooterItemButton<number>(
            [2, 4, 6],
            () => this.settings.difficulty,
            (x) => x.toString(),
            (x) => this.settings.difficulty = x,
            1,
            "bolt",
            "Difficulty of blockchain: How many leading zeroes block hash has to have",
            true
        );

        const rsaKeySize = new FooterItemButton<number>(
            [512, 1024, 2048, 4096],
            () => this.settings.openSslKeySize,
            (x) => x.toString(),
            (x) => this.settings.openSslKeySize = x,
            1,
            "key",
            "Size of RSA key. This size determines the maximum size of encrypted message"
        );

        const coinbaseAmount = new FooterItemButton<number>(
            [0, 5, 10, 25, 50, 75, 100],
            () => this.settings.coinbaseAmount,
            (x) => x.toString(),
            (x) => this.settings.coinbaseAmount = x,
            1,
            "money",
            "Coinbase size. How much money send to the miner?"
        );

        const accountSecrets = await this.accountSecrets.getAccountSecrets();

        const coinbaseAddress = new FooterItemButton<AccountSecret>(
            accountSecrets,
            () => accountSecrets.find(x => x.publicAddress === this.settings.coinbaseAddress),
            (x) => x.displayName,
            (x) => this.settings.coinbaseAddress = x.publicAddress,
            3,
            "address-book-o",
            "Coinbase address. Where to send newly generated money?"
        );

        const assSeenAs = new FooterItemButton<AccountSecret>(
            accountSecrets,
            () => accountSecrets.find(x => x.publicAddress === this.settings.asSeenBy),
            (x) => x.displayName,
            (x) => this.settings.asSeenBy = x.publicAddress,
            3,
            "eye-slash",
            "View the system as: ",
            true
        );

        return [difficulty,
            rsaKeySize,
            coinbaseAmount,
            coinbaseAddress,
            assSeenAs];

    }

}


