import { Component, Input, OnInit, ApplicationRef } from "@angular/core";

import { AccountSecret } from "../../models/account.secret";
import { AccountSecretsService } from "../../services/accountSecrets.service";
import { Settings } from "../../settings";
import { FooterItemButton } from "./footer-item";
import { ChangesInitiationService } from "app/services/changes-initiation.service";

@Component({
    selector: "app-footer-toolbox-item",
    templateUrl: "footer-toolbox-item.component.html",
    styleUrls: ["footer-toolbox-item.component.scss"]
})
export class FooterToolboxItemComponent implements OnInit {

    @Input() footerItem: FooterItemButton<any>;

    get displayName() {
        return this.footerItem.displayNameSelector(this.footerItem.currentValue());
    }

    constructor(private changesInitiation: ChangesInitiationService) { }


    updateValue(value) {
        this.footerItem.onSelected(value);

        if (this.footerItem.initChangeDetectionAfterUpdate === true) {
            this.changesInitiation.initiateUpdate();
            console.log("update tick")
        }
    }


    ngOnInit(): void { }
}