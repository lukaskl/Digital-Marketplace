import { ApplicationRef, ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ChangesInitiationService } from "app/services/changes-initiation.service";
import { InitializationService } from "app/services/initialization.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent implements OnInit {

  title = "Digital Marketplace";

  reloading: boolean;


  constructor(private initializer: InitializationService,
    private appRef: ApplicationRef,
    private cd: ChangeDetectorRef,
    private changesInitiation: ChangesInitiationService,
    private router: Router) {
    this.reloading = false;
  }

  async ngOnInit(): Promise<void> {
    await this.initializer.loadMocks();

    //TODO: remove this part and replace with a better solution
    // this is hack to refesh app after settings were changed
    this.changesInitiation.changeInitiationIsNecessary.subscribe(_ => {
      if (_ === true) {
        //https://stackoverflow.com/a/35106069/2357702
        //https://stackoverflow.com/a/44580036/2357702
        var currentUrl = this.router.url;
        var refreshUrl = currentUrl.indexOf('events') > -1 ? '/blockchain' : '/events';
        this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));

      }
    });
  }
}


