import { Component, OnInit } from "@angular/core";
import { InitializationService } from "app/services/initialization.service";

@Component({
  selector: "app-header",
  templateUrl: "header.component.html",
  styleUrls: ["header.component.scss"]
})
export class HeaderComponent {

  title = "Digital Marketplace";

  constructor() { }

}


