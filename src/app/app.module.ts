import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { AccountComponent } from "app/accounts/account.component";
import { AccountsCompositeComponent } from "app/accounts/accounts-composite.component";
import { AccountsComponent } from "app/accounts/accounts.component";
import { AddNewEventComponent } from "app/events/add-new/add-new.component";
import { EventComponent } from "app/events/event.component";
import { EventsComponent } from "app/events/events.component";
import { CoinbaseCreatedComponent } from "app/events/types/coinbase-created.component";
import { EventSignatureComponent } from "app/events/types/event-signature.component";
import { MoneyTransferredComponent } from "app/events/types/money-transferred.component";
import { TicketPurchasedComponent } from "app/events/types/ticket-purchased.component";
import { FooterToolboxItemComponent } from "app/root/footer/footer-toolbox-item.component";
import { AccountsService } from "app/services/accounts.service";
import { AccountSecretsService } from "app/services/accountSecrets.service";
import { BlockchainService } from "app/services/blockchain.service";
import { EventHandlersServices } from "app/services/event-handlers.service";
import { CoinbaseCreatedHandler } from "app/services/event-handlers/coinbase-created.handler";
import { MoneyTransferredHandler } from "app/services/event-handlers/money-transferred.handler";
import { EventsService } from "app/services/events.service";
import { InitializationService } from "app/services/initialization.service";
import { Settings } from "app/settings";

import { AccountSecretComponent } from "./accountSecrets/account-secret.component";
import { AccountSecretsComponent } from "./accountSecrets/account-secrets.component";
import { AppRoutingModule } from "./app-routing.module";
import { BlockComponent } from "./blockchain/block.component";
import { BlockchainComponent } from "./blockchain/blockchain.component";
import { AppComponent } from "./root/app.component";
import { FooterComponent } from "./root/footer/footer.component";
import { HeaderComponent } from "./root/header.component";
import { BlocksService } from "./services/blocks.service";
import { CryptoService } from "./services/crypto.service";
import { TicketAnnouncedHandler } from "./services/event-handlers/flight/ticket-announced.handler";
import { TicketCancelledHandler } from "./services/event-handlers/flight/ticket-cancelled.handler";
import { TicketPurchasedHandler } from "./services/event-handlers/flight/ticket-purchased.handler";
import { TicketsService } from "./services/tickets.service";
import { TicketComponent } from "./tickets/ticket.component";
import { TicketsComponent } from "./tickets/tickets.component";
import { ChangesInitiationService } from "app/services/changes-initiation.service";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FooterToolboxItemComponent,
    BlockComponent,
    BlockchainComponent,
    EventComponent,
    EventsComponent,
    AccountComponent,
    AccountsComponent,
    AccountSecretComponent,
    AccountSecretsComponent,
    AccountsCompositeComponent,
    MoneyTransferredComponent,
    CoinbaseCreatedComponent,
    TicketPurchasedComponent,
    EventSignatureComponent,
    AddNewEventComponent,
    TicketComponent,
    TicketsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ChangesInitiationService,
    AccountsService,
    AccountSecretsService,
    BlocksService,
    BlockchainService,
    EventsService,
    CryptoService,
    TicketsService,
    Settings,
    InitializationService,
    EventHandlersServices,
    MoneyTransferredHandler,
    CoinbaseCreatedHandler,
    TicketAnnouncedHandler,
    TicketPurchasedHandler,
    TicketCancelledHandler
    ],

  bootstrap: [AppComponent]
})
export class AppModule { }
