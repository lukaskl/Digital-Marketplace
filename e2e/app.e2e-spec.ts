import { DigitalMarketplacePage } from "./app.po";

describe("digital-marketplace App", () => {
  let page: DigitalMarketplacePage;

  beforeEach(() => {
    page = new DigitalMarketplacePage();
  });

  it("should display message saying app works", () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual("app works!");
  });
});
